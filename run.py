from app import app
import sys
from app.models import __init_app__

if __name__ == "__main__":
    if sys.argv[1:]:
        if sys.argv[1] == "init":
            __init_app__()
        elif sys.argv[1] == "run":
            app.run(debug=True)
    else:
        app.run(debug=True)
