# Gestion des stages et apprentissages

![Image text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTivu3J9udKnzIrxUEa4cpGZT4sa6IMYI8jV6aTkiNLLKeFo1JM448C50eVNQ1Sm-frLE0&usqp=CAU)

---

## Bienvenue sur le README de notre site web.

Dans le cadre du projet tutoré de Licence Professionnelle Métiers de l'informatique Conception, développement et test de logiciels, Spécialité : développement Web et mobile le sujet n°5 concerne la Gestion des stages/apprentissages des étudiants.

### Explication du projet

La gestion des stages à l’IUT d’Orléans est actuellement assurée par Arexis qui ne satisfait pas entièrement les enseignants et le personnel pédagogique.

### But du projet

Le but est de recréer un logiciel web permettant la gestion des stages et apprentissages des étudiants en Informatique de l'IUT d'Orléans.

### Les membres

- GHIZLAN Moqim @moqim.ghizlan (CHEF DU PROJET)
- SENTEIN Mathieu @mathieu.sentein
- CHICOT Florian @florian-chicot

## Installer et initialiser le projet

- Pour cloner le projet dans le répertoire de votre choix avec la commande :

```
$ git clone https://gitlab.com/moqim.ghizlan/gestion-des-stage-et-apprentissages.git
```

- Créer un virtualenv dans le dossier /gestion-des-stages-et-apprentissages et lancez la commande :

```
$ virtualenv env
```

pour l'activater :

```
$ source env/bin/activate
```

- Télécharger les packages :

```
$ pip install -r requirements.txt
```

- Créer la base de données et lancer flask :

```
$ python run.py init # pour remplir les tables avec les infos de base (eleve@eleve.eleve, ....).

$ python run.py run

$ python run.py # pour lancer le serveur en mode normal
```

- Enfin lancez votre navigateur de recherche préférée et écrivez dans l'URL :

```
$ localhost:5000
```

- (Optionnel) Lancer tailwind :

```
 $ cd tailwindcss
 $ npm install
 $ npx tailwindcss -i ./src/input.css -o ../app/static/styles/output.css --watch
```

## Explications des rôles :

Le rôle `teacher` correspond aux professeurs. Un professeur peut suivre les étudiants dans leur candidature. Quand une nouvelle offre est publiée il peut l'accepter ou la refuser. Quand une mission est envoyée par un étudiant il peut la valider ou non.
Le rôle `admin` correspond au secrétariat, qui ont tout les droits. Ce rôle peut faire les mêmes actions que le `teacher`, il peut également gérer les problèmes signalés par les utilisateurs.
Le rôle `user` correspond aux étudiants. Un étudiant peut publier une offre de stage qui sera soumis à validation par un `teacher`. Il peut candidater à une offre. Une candidature à trois états : en cours, accepté ou refusé. Une fois accepté par l'entreprise puis par un `teacher` pour une offre l'étudiant peur passer à l'étape de pré-remplissage de la convention.

## Suivi du projet :

Au moment de la lancement du projet, un compte d'admin sera ajouter par défaut, et c'est à l'admin d'ajouter les promotions et les responsables de ces promotions.
Le compte d'admin est :

```
$ numéro : admin
$ mdp : theadmin
```
