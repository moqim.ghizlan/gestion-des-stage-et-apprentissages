/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    // "./src/*.{html,js,css}",
    "../app/templates/*.html",
    "../app/templates/*/*.html",
  ],
  /*
  safelist: [
    {
      pattern: /./,
    },
  ],
  */
  theme: {
    extend: {},
  },
  plugins: [],
};
