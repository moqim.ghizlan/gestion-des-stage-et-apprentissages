function toggleLayer(trigger) {
  const querySelector = trigger.getAttribute("data-layer-selector");
  const layer = document.querySelector(querySelector);
  layer.classList.toggle("hidden");

  const isFullscreen = layer.classList.contains("fullscreen-layer");
  console.log(isFullscreen);

  if (isFullscreen) {
    document.querySelector("#background-layer").classList.toggle("hidden");
  }
}


function closeLayers() {
  const displayedLayers = document.querySelectorAll(".layer:not(.hidden)");
  for (let displayedLayer of displayedLayers)
    displayedLayer.classList.add("hidden");
}

function order(el) {
  if (
    document.querySelector("#orders>.bg-blue-100") &&
    document.querySelector("#orders>.bg-blue-100") != el
  )
    document
      .querySelector("#orders>.bg-blue-100")
      .classList.remove("bg-blue-100");
  el.classList.toggle("bg-blue-100");

  refresh();
}

var trsGL = [];

function refresh() {
  const search = document.querySelector("#search").value.toLowerCase();

  if (!trsGL.length) {
    if (document.querySelector(".item"))
      trsGL = [...document.querySelectorAll(".item")];
    else {
      trsGL = [...document.querySelectorAll("table tr")];
      trsGL.shift();
    }
  }
  let trs = trsGL;

  const order = document.querySelector("#orders>.bg-blue-100")
    ? document.querySelector("#orders>.bg-blue-100").value
    : null;

  const invert = document.querySelector("#orders>.bg-blue-100")
    ? document.querySelector("#orders>.bg-blue-100").hasAttribute("invert")
    : null;

  trs = trs
    .map((tr) => {
      const val = tr
        .querySelector('[name="1"]')
        .innerText.trim()
        .toLocaleLowerCase();
      if (val.includes(search)) return tr;
    })
    .filter((n) => n);

  if (order) {
    trs.sort((a, b) => {
      const valA = a
        .querySelector(`[name="${order}"]`)
        .innerText.trim()
        .toLocaleLowerCase();
      const valB = b
        .querySelector(`[name="${order}"]`)
        .innerText.trim()
        .toLocaleLowerCase();

      if (invert) return valA > valB ? -1 : 1;
      else return valA < valB ? -1 : 1;
    });
  }

  let HTML = "";
  for (let tr of trs) HTML += tr.outerHTML;

  if (document.querySelector("#info")) document.querySelector("#info").remove();
  if (HTML.length == 0)
    if (!document.querySelector("#items"))
      document
        .querySelector("table")
        .insertAdjacentHTML(
          "afterend",
          `<em id="info" class="text-sm flex justify-center mt-2">Aucune entrée trouvée.</em>`
        );
    else
      document.querySelector(
        "#items"
      ).innerHTML = `<em id="info" class="text-sm flex justify-center mt-2">Aucune entrée trouvée.</em>`;

  if (!document.querySelector("#items")) {
    document.querySelector("tbody").outerHTML =
      document.querySelector("tbody>tr").outerHTML;
    document.querySelector("tbody").insertAdjacentHTML("beforeend", HTML);
  } else {
    document.querySelector("#items").innerHTML = HTML;
  }
}

function removeHidden(id){ 
  // revove hidden input (use in modify room)
  const li = document.getElementById('li'+id);
  const p = li.querySelector('p');
  const form = li.querySelector('.edit-form');
  p.classList.add('hidden');
  form.classList.remove('hidden');
}
