from flask_login import LoginManager, UserMixin
from flask import Flask
from .views import app

from . import models
from .models import User
models.db.init_app(app)
# from .models import db
login_manager = LoginManager()
login_manager.login_view = 'signin'
login_manager.init_app(app)


@login_manager.user_loader
def load_user(email):
    return User.query.get(str(email))
