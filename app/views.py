from calendar import c
from flask import Flask, render_template, flash, redirect, request, url_for, session, jsonify, make_response
from flask_login import UserMixin, login_user, current_user, logout_user, login_required
from .app import app, db, api
from flask_restful import Resource
from werkzeug.security import generate_password_hash, check_password_hash
from .models import *
from werkzeug.utils import secure_filename
import json
import os
import urllib.request
import tempfile
import datetime
from os.path import join, dirname, realpath
from typing import final
from sqlalchemy import or_
from docx import Document
from datetime import datetime
from collections import defaultdict


###################################################
###################################################
###################################################


#                       auth


###################################################
###################################################
###################################################

@app.route("/signin", methods=['GET', 'POST'])
def signin():
    # if current_user.is_authenticated(): return redirect(url_for('index'))
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        user = User.query.filter_by(numero=username).one_or_none()
        if user != None:
            if check_password_hash(user.password, password):
                if password == username:
                    login_user(user)
                    return redirect(url_for('set_teacher_password'))
                else:
                    login_user(user)
                    return redirect(url_for('index'))
            return render_template("login.html", error="Les identifiants ne sont pas corrects.")
        return render_template("login.html", error="Les identifiants ne sont pas corrects.")
    return render_template("login.html")


@app.route("/account/reset", methods=['GET', 'POST'])
def reset_password():
    return render_template("forgotten_password.html")


@app.route("/signup", methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        fname = request.form.get('fname')
        lanme = request.form.get('lanme')
        email = request.form.get('email')
        numero = request.form.get('numero')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')
        promo_id = request.form['promo']
        is_initial = False
        if request.form.get("year_type") == None:
            is_initial = True
        else:
            is_initial = False
        print("\n"*5)
        print(is_initial)
        print("\n"*5)

        email_verf = User.query.filter_by(email=email).one_or_none()
        numero_verf = User.query.filter_by(numero=numero).one_or_none()

        if email_verf == None:  # email not reg
            if numero_verf == None:  # number not reg
                if password1 == password2:  # passwords match
                    user = User(
                        first_name=fname,
                        last_name=lanme,
                        email=email,
                        numero=numero,
                        password=generate_password_hash(
                            password1, method='sha256'),
                        role_id=1  # 1 == user
                    )
                    db.session.add(user)
                    db.session.commit()
                    belong = Belong(user_id=user.id, promo_id=promo_id, is_initiale=is_initial)
                    db.session.add(belong)
                    db.session.commit()
                    login_user(user)
                    flash('Bienvenu !')
                    return redirect(url_for('index'))
                else:
                    return render_template("signup.html", error="Les mots de passe ne correspondent pas.")
            else:
                return render_template("signup.html", error="Ll existe déjà un compte lié à ce numéro.")
        else:
            return render_template("signup.html", error="Ll existe déjà un compte lié à cette adresse mail.")
    return render_template("signup.html")


@app.route("/logout", methods=['GET', 'POST'])
def logout():
    logout_user()
    # flash('Au revoir !')
    return redirect(url_for('signin'))


@app.route("/teacher/account/set-password", methods=['GET', 'POST'])
def set_teacher_password():
    # if current_user.is_authenticated : return redirect(url_for('index'))
    if request.method == 'POST':
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')
        if password1 != password2:
            return render_template("set_teacher-password.html", error="Les mots de passe ne correspondent pas.")
        if len(password1) <= 6:
            return render_template("set_teacher-password.html", error="Le mot de passe doit etre au moins  6 caracteres.")
        user = User.query.filter_by(numero=current_user.numero).one_or_none()
        user.password = generate_password_hash(password1, method='sha256')
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    return render_template("set_teacher-password.html", error="Veuillez choisir votre mot de passe.")


@app.route("/taecher/add-new", methods=['GET', 'POST'])
def add_new_teacher():
    if request.method == 'POST':
        teacher_fname = request.form.get('taecher_fname')
        teacher_lname = request.form.get('taecher_lname')
        teacher_email = request.form.get('taecher_email')
        teacher_numero = request.form.get('taecher_numero')
        if User.query.filter_by(email=teacher_email).one_or_none():
            return render_template("add_new_teacher.html", error="L'adresse mail existe déjà.")
        if User.query.filter_by(numero=teacher_numero).one_or_none():
            return render_template("add_new_teacher.html", error="Le numéro existe déjà.")
        u = User(first_name=teacher_fname, last_name=teacher_lname, email=teacher_email,
                 numero=teacher_numero, password=generate_password_hash(teacher_numero, method='sha256'), role_id=2)
        db.session.add(u)
        db.session.commit()
    return redirect(url_for('add_promo'))


###################################################
###################################################
###################################################


#                       GENERAL


###################################################
###################################################
###################################################


@app.route("/", methods=['GET', 'POST'])
@app.route("/index", methods=['GET', 'POST'])
@app.route("/home", methods=['GET', 'POST'])
@login_required
def index():
    if current_user.role_id == 1:  # est un etudiant
        offres = Offre.query.filter_by(is_accepted=True).all()
        return render_template("index.html", data=offres)
    elif current_user.role_id == 2:  # est un prof
        # if request.method == 'POST':
        data = Belong.query.filter_by().all()
        return render_template("index_teacher.html", data=data)
    else:  # admin
        promos = Promo.query.all()
        for promo in promos:
            promo.nb_validation = 0
            belong = Belong.query.filter_by(promo_id=promo.id).all()
            for b in belong:
                if b.user.status_search == 2:
                    promo.nb_validation += 1

        return render_template("index_admin.html", promos=promos)


@app.route("/students/q=<string:q>", methods=['GET', 'POST'])
@login_required
def search_students(q):
    if q == "stage":
        data = Belong.query.filter_by(is_initiale=False).all()
        return render_template("index_teacher.html", data=data)
    elif q == "apprenti":
        data = Belong.query.filter_by(is_initiale=True).all()
        return render_template("index_teacher.html", data=data)
    elif q == "all":
        data = Belong.query.all()
        return render_template("index_teacher.html", data=data)
    else:
        return redirect(url_for('index'))


@app.route("/student/search_by_promo/<promo_id>", methods=['GET', 'POST'])
@login_required
def search_by_promo(promo_id):
    promo_id = int(promo_id)
    data = Belong.query.filter_by(promo_id=promo_id).all()
    return render_template("index_teacher.html", data=data)


@app.route("/user/profil", methods=['GET', 'POST'])
@login_required
def profil():
    histories = History.query.filter_by(user_id=current_user.id).all()
    user = current_user
    user.has_mission_request = None
    user.promo = None
    if user.role_id == 1:
        user.promo = Belong.query.filter_by(user_id=user.id).first()
        user.has_mission_request = Mission_request.query.filter_by(
            user_id=user.id).one_or_none()
    elif user.role_id == 2:
        user.promo = Manage.query.filter_by(user_id=user.id).first()
    return render_template("profil.html", histories=histories, user=user, connected=True)


@app.route("/user/<int:id>/profil/", methods=['GET', 'POST'])
@login_required
def user_profil(id):
    if current_user.role_id == 1:
        return redirect(url_for('profil'))
    user = User.query.filter_by(id=id).first()
    histories = History.query.filter_by(user_id=user.id).all()
    user.promo = None
    if user.role_id == 1:
        user.promo = Belong.query.filter_by(user_id=user.id).first()
    elif user.role_id == 2:
        user.promo = Manage.query.filter_by(user_id=user.id).first()
    return render_template("profil.html", histories=histories, user=user)


###################################################
###################################################
###################################################


#                     OFFRES


###################################################
###################################################
###################################################

@app.route("/offer/list", methods=['GET', 'POST'])
def list_offers():
    offres = Offre.query.filter_by(is_accepted=True).all()
    # get nb historics for each offre
    for offre in offres:
        offre.nb_historics = History.query.filter_by(offre_id=offre.id).count()
    if current_user.role_id != 1:
        return render_template("list_teachers-offer.html", data=offres)
    return render_template("index.html", data=offres, user=current_user)


@app.route("/offre/add", methods=['GET', 'POST'])
def new_offer():
    is_sudo = False
    if current_user.role_id == 1:
        is_sudo = False
    else:
        is_sudo = True
    if request.method == 'POST':
        titre = request.form.get('titre')
        resumerMission = request.form.get('resumerMission')
        description = request.form.get('description')
        weeks_number = request.form.get('weeks_number')
        company_id = request.form.get('company')
        offre_type_id = request.form.get('offre_type')
        offre = Offre(
            titre=titre,
            resumerMission=resumerMission,
            description=description,
            weeks_number=weeks_number,
            company_id=company_id,
            offre_type_id=offre_type_id,
            is_accepted=is_sudo,
        )
        db.session.add(offre)
        db.session.commit()
        type_offre = None
        if offre_type_id == '1':
            type_offre = "STA"
        elif offre_type_id == '2':
            type_offre = "APP"
        offre.reference = f'@REF{type_offre}ID{offre.id}'
        db.session.add(offre)
        db.session.commit()
        return redirect(url_for('offre', id=offre.id))
    return render_template("add_offer.html")


@app.route("/offre/<id>", methods=['GET', 'POST'])
@login_required
def offre(id):
    offre = Offre.query.filter_by(id=id).one_or_none()
    if not offre:
        return redirect(url_for('page_404'))
    offre.nb_historics = History.query.filter_by(offre_id=offre.id).count()
    offre.i_applied = False
    if current_user.role_id == 1:
        offre.i_applied = History.query.filter_by(
            user_id=current_user.id,
            offre_id=offre.id
        ).one_or_none() != None
    return render_template("offer.html", offre=offre)


@app.route("/offre/<id>/apply", methods=['GET', 'POST'])
@login_required
def apply(id):
    offre = Offre.query.filter_by(id=id).one_or_none()
    if not offre:
        return redirect(url_for('page_404'))
    return render_template("apply.html", offre=offre)


@app.route("/offre/<id>/apply/done", methods=['GET', 'POST'])
@login_required
def apply_done(id):
    if am_i_stagiaire() != Offre.query.filter_by(id = id).first().offre_type.id:
        return redirect(url_for('page_404'))
    from datetime import datetime
    now = datetime.now()  # current date and time
    offre = Offre.query.filter_by(id=id).first()
    history = History(
        user_id=current_user.id,
        offre_id=offre.id,
        company_id=offre.company.id,
        send_date=now.strftime("%d/%m/%Y"),
        anser=1
    )
    db.session.add(history)
    db.session.commit()
    user = User.query.filter_by(id=current_user.id).first()
    user.status_search = 1
    db.session.commit()

    return redirect(url_for('index'))


@app.route("/offre/<id>/delete", methods=['GET', 'POST'])
@login_required
def delete_offre(id):
    offre = Offre.query.filter_by(id=id).one_or_none()
    if not offre:
        return redirect(url_for('index'))
    db.session.delete(offre)
    db.session.commit()
    return redirect(url_for('list_offers'))


@app.route("/offre/<id>/edit", methods=['GET', 'POST'])
@login_required
def edit_offre(id):
    offre = Offre.query.filter_by(id=id).one_or_none()
    if not offre:
        return redirect(url_for('index'))
    if request.method == 'POST':
        titre = request.form.get('titre')
        description = request.form.get('description')
        weeks_number = request.form.get('weeks_number')
        company_id = request.form.get('company')
        offre_type_id = request.form.get('offre_type')
        offre.titre = titre
        offre.description = description
        offre.weeks_number = weeks_number
        offre.company_id = company_id
        offre.offre_type_id = offre_type_id
        type_offre = None
        if offre_type_id == '1':
            type_offre = "STA"
        elif offre_type_id == '2':
            type_offre = "APP"
        offre.reference = f'@REF{type_offre}ID{offre.id}'
        db.session.commit()
        return redirect(url_for('offre', id=offre.id))
    return render_template("add_offer.html", offre=offre)


@app.route("/offre/<int:offre_id>/accept/confirmation/<string:object_type>", methods=['GET', 'POST'])
@login_required
def accept_offre_confirmation(offre_id, object_type):
   if current_user.role_id == 1:
       return redirect(url_for('index'))
   return render_template(
       "accept_confirmation.html",
       offre_id=offre_id,
       object_type=object_type
   )


@app.route("/offre/<int:offre_id>/accept", methods=['GET', 'POST'])
@login_required
def accept_offre(offre_id):
    if current_user.role_id == 1:
        return redirect(url_for('index'))
    offre = Offre.query.filter_by(id=offre_id).one_or_none()
    if not offre:
        return redirect(url_for('index'))
    offre.is_accepted = True
    db.session.commit()
    return redirect(url_for('offre', id=offre_id))


@app.route("/offre/sort/filter=<string:filter>", methods=['GET', 'POST'])
@login_required
def superuser_sort_offres(filter):
    if current_user.role_id == 1:
        return redirect(url_for('index'))
    if filter == 'stage':
        offres = Offre.query.filter_by(offre_type_id=1).all()
        return render_template("list_teachers-offer.html", data=offres)
    if filter == 'apprentissage':
        offres = Offre.query.filter_by(offre_type_id=2).all()
        return render_template("list_teachers-offer.html", data=offres)
    else:
        offres = Offre.query.all()
        return render_template("list_teachers-offer.html", data=offres)


###################################################
###################################################
###################################################


#                     company


###################################################
###################################################
###################################################

@app.route("/company/activities/edit", methods=['GET', 'POST'])
@login_required
def edit_company_activities():
    # companies = Company.query.all()
    return render_template("edit_company-activities.html")


@app.route("/companies/list", methods=['GET', 'POST'])
@login_required
def list_company():
    companies = Company.query.all()
    return render_template("list_company.html", companies=companies)


@app.route("/company/<int:id>/offres", methods=['GET', 'POST'])
@login_required
def company_offres(id):
    offres = Offre.query.filter_by(company_id=id).all()
    return render_template("index.html", data=offres)


@app.route('/company/add', methods=['GET', 'POST'])
@login_required
def add_company():
    if current_user.role_id == 1:
        return redirect(url_for('page_404'))
    if request.method == 'POST':
        name = request.form.get('name')
        contact_email = request.form.get('contact_email')
        contact_phone = request.form.get('contact_phone')
        address = request.form.get('address')
        ville = request.form.get('ville')
        cpostal = request.form.get('cpostal')
        siret = request.form.get('siret')
        multiselect = request.form.getlist('compnay-activites')

        first_name = request.form.get('first_name')
        last_name = request.form.get('last_name')
        email = request.form.get('email')
        phone_number = request.form.get('phone_number')
        responsable = Responsable(
            first_name=first_name,
            last_name=last_name,
            email=email,
            phone_number=phone_number
        )
        db.session.add(responsable)
        db.session.commit()
        company = Company(
            name=name,
            contact_email=contact_email,
            contact_phone=contact_phone,
            address=address,
            responsable_id=responsable.id,
            ville=ville,
            cpostal=cpostal,
            siret=siret
        )
        db.session.add(company)
        db.session.commit()
        responsable.idCompany=company.id
        db.session.add(responsable)
        db.session.commit()
        for activity in multiselect:
            do_activity = Do_activity(
                company_id=company.id,
                activity_id=activity
            )
            db.session.add(do_activity)
            db.session.commit()
        return redirect(url_for('company', id=company.id))
    return render_template("add-edit_company.html")


@app.route('/company/add/new', methods=['GET', 'POST'])
def add_new_company():
    if request.method == 'POST':
        company_name = request.form.get('company_name')
        company_siret = request.form.get('company_siret')
        company_city = request.form.get('company_city')
        company_address = request.form.get('company_address')
        company_cpostal = request.form.get('company_cpostal')
        company_mail = request.form.get('company_mail')
        company_num_phone = request.form.get('company_num_phone')
        company_resp_fname = request.form.get('company_resp_fname')
        company_resp_lname = request.form.get('company_resp_lname')
        company_resp_mail = request.form.get('company_resp_mail')
        company_resp_num_phone = request.form.get('company_resp_num_phone')
        multiselect = request.form.getlist('compnay-activites')
        if Company.query.filter_by(name=company_name).one_or_none():
            return redirect(url_for('page_404'))
        responsable = Responsable(
            first_name=company_resp_fname,
            last_name=company_resp_lname,
            email=company_resp_mail,
            phone_number=company_resp_num_phone
        )
        db.session.add(responsable)
        db.session.commit()
        company = Company(
            name=company_name,
            siret=company_siret,
            ville=company_city,
            address=company_address,
            cpostal=company_cpostal,
            contact_email=company_mail,
            contact_phone=company_num_phone,
            responsable_id=responsable.id
        )
        addIdCompany = Responsable.query.filter_by(id=responsable.id).first()
        addIdCompany.idCompany=company.id
        db.session.commit()
        db.session.add(company)
        responsable.idCompany=company.id
        db.session.add(responsable)
        db.session.commit()
        for activity in multiselect:
            do_activity = Do_activity(
                company_id=company.id,
                activity_id=activity
            )
            db.session.add(do_activity)
            db.session.commit()
        return redirect(url_for('company', id=company.id))


@app.route('/company/<int:id>/edit', methods=['GET', 'POST'])
@login_required
def edit_company(id):
    if current_user.role_id == 1:
        return redirect(url_for('page_404'))
    if request.method == 'POST':
        name = request.form.get('name')
        contact_email = request.form.get('contact_email')
        contact_phone = request.form.get('contact_phone')
        address = request.form.get('address')

        first_name = request.form.get('first_name')
        last_name = request.form.get('last_name')
        email = request.form.get('email')
        phone_number = request.form.get('phone_number')
        activites = request.form.getlist('activites')
        ville = request.form.get('ville')
        cpostal = request.form.get('cpostal')
        siret = request.form.get('siret')

        company = Company.query.filter_by(id=id).first()
        responsable = Responsable.query.filter_by(
            id=company.responsable_id).first()
        # activites_company = Do_activity.query.filter_by(company_id = company.id).all()
        # for activ in activites:
        #     act = Activity.query.filter_by(id = activites_company.id).first()

        responsable.first_name = first_name
        responsable.last_name = last_name
        responsable.email = email
        responsable.phone_number = phone_number
        db.session.commit()
        company.name = name
        company.contact_email = contact_email
        company.contact_phone = contact_phone
        company.address = address
        company.ville = ville
        company.cpostal = cpostal
        company.siret = siret
        db.session.commit()
        return redirect(url_for('company', id=company.id))

    return render_template("add-edit_company.html", company=Company.query.filter_by(id=id).first())


@app.route('/activity/add/user-type=<string:userT>', methods=['GET', 'POST'])
@login_required
def new_activity(userT):
    if request.method == 'POST':
        domaine = request.form.get('domaine-name')
        if Activity.query.filter_by(domaine=domaine).one_or_none():
            return render_template('add_activity.html', errer="L'activité existe déjà")
        activity = Activity(
            domaine=domaine
        )
        db.session.add(activity)
        db.session.commit()
        if userT == "sudo":
            return redirect(url_for('add_company'))
        return redirect(url_for('new_offer'))
    return render_template('add_activity.html')


@app.route('/company/<int:id>/<string:action>', methods=['GET', 'POST'])
@login_required
def action_company(id, action):
    if current_user.role_id == 1:
        return redirect(url_for('page_404'))
    company = Company.query.filter_by(id=id).first()
    if action == "delete":
        db.session.delete(company)
        db.session.commit()
        return redirect(url_for('list_company'))
    elif action == "accept":
        # company.is_accepte = 1
        db.session.commit()
        return redirect(url_for('list_company'))
    elif action == "refuse":
        # company.is_accepte = 2
        db.session.commit()
        return redirect(url_for('list_company'))
    else:
        return redirect(url_for('page_404'))


@app.route("/company/<id>", methods=['GET', 'POST'])
@login_required
def company(id):
    company = Company.query.filter_by(id=id).one_or_none()
    if company == None:
        return redirect(url_for('page_404'))

    activities = Do_activity.query.filter_by(company_id=id).all()

    return render_template("company.html", company=company, activities=activities)

###################################################
###################################################
###################################################


#                   poromotion


###################################################
###################################################
###################################################


@app.route('/promotion/add', methods=['GET', 'POST'])
@login_required
def add_promo():
    if current_user.role_id != 3:
        return redirect(url_for('page_404'))
    if request.method == 'POST':
        name = request.form.get('name')
        manager = int(request.form.get('manager'))
        user = User.query.filter_by(id=manager).first()
        promo = Promo(name=name)
        db.session.add(promo)
        db.session.commit()
        manage = Manage(user_id=user.id, promo_id=promo.id)
        db.session.add(manage)
        db.session.commit()
        return redirect(url_for('index'))
    return render_template("add-edit_promo.html")


@app.route('/promotion/<int:id>/edit', methods=['GET', 'POST'])
@login_required
def edit_promo(id):
    if current_user.role_id != 3:
        return redirect(url_for('page_404'))
    if request.method == 'POST':
        name = request.form.get('name')
        responsable_id = int(request.form.get('manager'))
        user = User.query.filter_by(id=responsable_id).first()
        promo = Promo.query.filter_by(id=id).first()

        manage = Manage.query.filter_by(promo_id=promo.id).first()
        promo.name = name
        db.session.commit()
        manage.user_id = int(responsable_id)
        db.session.commit()

        return redirect(url_for('index'))
    return render_template("add-edit_promo.html", promo=Promo.query.filter_by(id=id).first())


@app.route('/promotion/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_promo(id):
    if current_user.role_id != 3:
        return redirect(url_for('page_404'))
    promo = Promo.query.filter_by(id=id).first()
    db.session.delete(promo)
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/promotion/<int:id>', methods=['GET', 'POST'])
@login_required
def promo(id):
    if current_user.role_id != 3:
        return redirect(url_for('page_404'))
    return render_template(
        "promotion.html",
        promo=Promo.query.filter_by(id=id).first()
    )


@app.route('/promotion/<int:id>/students', methods=['GET', 'POST'])
@login_required
def promo_students(id):
    if current_user.role_id == 1:
        return redirect(url_for('page_404'))
    promo = Belong.query.filter_by(promo_id=id).all()
    return render_template('index_teacher.html', data=promo)


@app.route('/promotion/manage', methods=['GET', 'POST'])
@login_required
def promo_manage():
    if current_user.role_id != 2:
        return redirect(url_for('page_404'))
    promo = Manage.query.filter_by(user_id=current_user.id).first()
    return render_template("promotion.html", promo=promo)


@app.errorhandler(404)
@app.route("/404", methods=['GET', 'POST'])
def page_404():
    return render_template("404.html")


@app.route("/taecher/my/promotion", methods=['GET', 'POST'])
def my_promo():
    if current_user.role_id != 2:
        return redirect(url_for('page_404'))
    i = Manage.query.filter_by(user_id=current_user.id).first()
    promo = Belong.query.filter_by(promo_id=i.promo_id).all()
    return render_template('index_teacher.html', data=promo)


###################################################
###################################################
###################################################


#                    PROBLEM


###################################################
###################################################
###################################################

@app.route('/problem/add', methods=['GET', 'POST'])
@login_required
def add_problem():
    if request.method == 'POST':
        titre = request.form.get('titre')
        description = request.form.get('description')
        link = request.form.get('url')
        reporter_id = current_user.id
        new_problem(titre=titre, description=description,
                    link=link, reporter_id=reporter_id)
        return redirect(url_for('index'))
    return render_template("add_problem.html")


@app.route('/problem/<int:id>', methods=['GET', 'POST'])
@login_required
def problem(id):
    if current_user.role_id != 3:
        return redirect(url_for('page_404'))
    problem = Problem.query.filter_by(id=id).one_or_none()
    if not problem:
        return redirect(url_for('page_404'))
    if problem.is_solved:
        solved = Solved.query.filter_by(problem_id=problem.id).first()
        return render_template("problem.html", problem=problem, solved=solved)
    return render_template("problem.html", problem=problem)


@app.route('/problems/list', methods=['GET', 'POST'])
@login_required
def list_problems():
    if current_user.role_id != 3:
        return redirect(url_for('page_404'))
    problems = Problem.query.order_by(Problem.is_solved).all()
    return render_template("list_problems.html", problems=problems)


@app.route('/problem/<int:id>/solved', methods=['GET', 'POST'])
@login_required
def solve_problem(id):
    if current_user.role_id != 3:
        return redirect(url_for('page_404'))
    problem = Problem.query.filter_by(id=id).first()
    if not problem:
        return redirect(url_for('page_404'))
    problem.is_solved = True
    db.session.commit()
    solve = Solved(problem_id=problem.id, solver_id=current_user.id)
    db.session.add(solve)
    db.session.commit()
    return redirect(url_for('problem', id=problem.id))


@app.route('/problem/sort/filter=<string:filter>', methods=['GET', 'POST'])
@login_required
def sort_pb(filter):
    if current_user.role_id != 3:
        return redirect(url_for('page_404'))
    if filter == "solved":
        return render_template("list_problems.html", problems=Problem.query.filter_by(is_solved=True).all())
    if filter == "no-solved":
        return render_template("list_problems.html", problems=Problem.query.filter_by(is_solved=False).all())
    return redirect(url_for('list_problems'))


@app.route('/convention/generate', methods=['GET', 'POST'])
@login_required
def generate_convention():
    # /api/offre/<string:offre_ref>/exists
    if current_user.role_id != 1:
        return redirect(url_for('index'))
    mr = Mission_request.query.filter_by(user_id=current_user.id).one_or_none()
    if not mr:
        return redirect(url_for('profil'))
    if not mr.accepted:
        return redirect(url_for('profil'))

    if request.method == 'POST':
        data = request.form.get

        items = {
            'NOMORG': data('company_name'),
            'ADDRORG': data('company_address'),
            'RESPORG': data('responsable_name'),
            'POSTRORG': data('responsable_poste'),
            'TELORG': data('responsable_number'),
            'MAILORG': data('responsable_email'),
            'LIEUORG': data('company_city'),

            'NOMSTA': data('stagiaire_fname') + ' ' + data('stagiaire_lname'),
            'SHOM': 'x' if data('stagiaire_sex') == 'men' else '',
            'SFEM': 'x' if data('stagiaire_sex') == 'women' else '',
            'STANAI': data('stagiaire_birth_date'),
            'TELSTA': data('stagiaire_phone'),
            'MAILSTA': data('stagiaire_email'),
            'ADDRSTA': data('stagiaire_address'),

            'DATEDEBSTA': data('stage_date_debut'),
            'DATEFINSTA': data('stage_date_fin'),
            'DUREESTASEM': data('stage_weeks_duration'),
            'DUREESTAJOU': data('stage_days_duration'),

            'ENSREF': data('name_teacher'),
            'MAILENSREF': data('email_teacher'),

            'NOMTUTSTAGE': data('tuteur_name'),
            'MAILTUTSTAGE': data('tuteur_email'),

            'LIEUCONV': data('convention_city'),
            'DATECONV': data('convention_date'),

            'REPETA': data('name_representant'),

            'HORSAL': data('stage_hour_salary'),
            'WEEKHOUR': data('stage_week_hours'),
        }

        document = Document("modele-convention.docx")

        for t in document.tables:
            for r in t.rows:
                for c in r.cells:
                    for p in c.paragraphs:
                        for key, val in items.items():
                            if key in p.text:
                                print(val)
                                p.text = p.text.replace(key, val)

        for p in document.paragraphs:
            for key, val in items.items():
                if key in p.text:
                    p.text = p.text.replace(key, val)

        docname = current_user.numero
        document.save(
            "app/static/conventions/" + docname + ".docx")

        conv1 = Convention(
            user_id=current_user.id,
            company_id=mr.company_id,
            mission_request_id=mr.id,
            status=0)
        db.session.add(conv1)
        db.session.commit()
        return redirect(url_for('index'))
    user = current_user

    # offre = Offre.query.filter_by(reference=refoffre).one_or_none()
    # company = Company.query.filter_by(id=offre.id).first()
    # print(offre.company)

    return render_template('generate_convention.html', user=user, offre=offre)


@app.route('/conventions/list', methods=['GET', 'POST'])
def list_conventions():
    if current_user.role_id != 3:
        return redirect(url_for('index'))
    conventions = Convention.query.all()
    for convention in conventions:
        user = User.query.filter_by(id=convention.user_id).first()
        user.promo = Belong.query.filter_by(user_id=user.id).first()
    return render_template('list_conventions.html', conventions=conventions)


@app.route('/conventions/id=<int:id>', methods=['GET', 'POST'])
def convention(id):
    if current_user.role_id != 3:
        return redirect(url_for('index'))
    convention = Convention.query.filter_by(id=id).first()
    return render_template('convention.html', convention=convention)


@app.route('/my-convention', methods=['GET', 'POST'])
def see_my_convention():
    convention = Convention.query.filter_by(user_id=current_user.id).first()
    if not convention:
        return redirect(url_for('profil'))
    return render_template('convention.html', convention=convention)


@app.route('/my-convention/delete', methods=['GET', 'POST'])
def delete_my_convention():
    convention = Convention.query.filter_by(user_id=current_user.id).first()
    if not convention:
        return redirect(url_for('profil'))
    db.session.delete(convention)
    db.session.commit()
    return redirect(url_for('profil'))


@app.route('/convention/id=<int:id>/chnage-status<string:new_status>', methods=['GET', 'POST'])
def convention_status(id, new_status):
    if current_user.role_id != 3:
        return redirect(url_for('index'))
    convention = Convention.query.filter_by(id=id).first()
    if not convention:
        return redirect(url_for('index'))
    new_s = 0
    if new_status == "accepte":
        new_s = 1
    elif new_status == "refuse":
        new_s = 2
    convention.status = new_s
    db.session.commit()
    return redirect(url_for('convention', id=convention.id))


@app.route('/students/<string:search_type>', methods=['GET', 'POST'])
def all_students_stage(search_type):
    if current_user.role_id != 3:
        return redirect(url_for('index'))
    users = list()
    if search_type == "stage":
        belongs = Belong.query.filter_by(is_initiale=True).all()
        for belong in belongs:
            user = belong.user
            user.is_initiale = belong.is_initiale
            user.nb_historic = History.query.filter_by(user_id=user.id).count()
            users.append(user)
    elif search_type == "apprentissage":
        belongs = Belong.query.filter_by(is_initiale=False).all()
        for belong in belongs:
            user = belong.user
            user.is_initiale = belong.is_initiale
            user.nb_historic = History.query.filter_by(user_id=user.id).count()
            users.append(user)
    else:
        belongs = Belong.query.all()
        for belong in belongs:
            user = belong.user
            user.is_initiale = belong.is_initiale
            user.nb_historic = History.query.filter_by(user_id=user.id).count()
            users.append(user)

    return render_template('list_students.html', users=users)


@app.route('/acceptation/offres', methods=['GET', 'POST'])
def offres_tobe_accepted():
    if current_user.role_id == 1:
        return redirect(url_for('page_404'))
    offres = Offre.query.filter_by(is_accepted=False).all()
    return render_template("list_teachers-offer.html", data=offres)


@app.route('/history/<int:id>edit', methods=['GET', 'POST'])
@login_required
def edit_history(id):
    final_option = None
    if current_user.role_id != 1:
        return redirect(url_for('page_404'))
    history = History.query.filter_by(id=id).first()
    if not history:
        return redirect(url_for('page_404'))
    else:
        if request.method == 'POST':
            select = request.form.get('rep-options')
            history.anser = select
            db.session.commit()
            user = User.query.filter_by(id=history.user_id).first()
            if select == "1":
                final_option = 1
            elif select == "2":
                final_option = 2
            else:
                final_option = 1

            user.status_search = final_option
            db.session.commit()

            if final_option == 2:
                offre = Offre.query.filter_by(id=history.offre_id).first()
                offre.is_token = True
                db.session.commit()

            return redirect(url_for('profil'))
    return render_template('edit_history.html', history=history)


@app.route('/students/apply/offre/<int:id>', methods=['GET', 'POST'])
@login_required
def student_apply_to_offre(id):
    if current_user.role_id == 1:
        return redirect(url_for('page_404'))
    offre = Offre.query.filter_by(id=id).one_or_none()
    if not offre:
        return redirect(url_for('page_404'))
    else:
        historics = History.query.filter_by(offre_id=id).all()
        belongs = []
        for historic in historics:
            belongs.append(
                Belong.query.filter_by(user_id=historic.user_id).first()
            )
        return render_template('index_teacher.html', data=belongs)


@app.route('/new/request/mission-confirmation/<int:id>', methods=['GET', 'POST'])
@login_required
def new_mission_confirmation_request(id):
    if current_user.role_id != 1:
        return redirect(url_for('page_404'))
    if request.method == 'POST':
        contrat_type = request.form.get('offre_type')
        missions = request.form.get('missions')
        idMaitreApprentiStage = request.form.get('idMaitreApprentiStage')
        duration = request.form.get('duration')
        company_id = request.form.get('company')
        start_at = request.form.get('start-at')
        end_at = request.form.get('end-at')
        salary = request.form.get('salary')
        user_id = current_user.id
        print('contrat_type', contrat_type)
        mission_request = Mission_request(
            offre_type_id=contrat_type,
            missions_id=missions,
            idMaitreApprentiStage=idMaitreApprentiStage,
            weeks_number=duration,
            start_at= datetime.strptime(start_at, '%Y-%m-%d'),
            end_at= datetime.strptime(end_at, '%Y-%m-%d'),
            user_id=user_id,
            company_id=company_id,
            salary = salary
        )
        db.session.add(mission_request)
        db.session.commit()
        return redirect(url_for('one_request_mission_confirmation', id=mission_request.id))
    offre = Offre.query.filter_by(id=id).first()

    return render_template('add_mission-request.html', offre=offre)


@app.route('/new/request/mission-confirmation/responsable/<int:id>', methods=['POST'])
def add_new_responsable(id):
    if request.method == 'POST':
        company_resp_fname = request.form.get('company_resp_fname')
        company_resp_lname = request.form.get('company_resp_lname')
        company_resp_mail = request.form.get('company_resp_mail')
        company_resp_num_phone = request.form.get('company_resp_num_phone')
        idCompany = request.form.get('company_resp_idCompany')
        responsable = Responsable(
            first_name=company_resp_fname,
            last_name=company_resp_lname,
            email=company_resp_mail,
            phone_number=company_resp_num_phone,
            idCompany=idCompany
        )
        db.session.add(responsable)
        db.session.commit()
    if Mission_request.query.filter_by(user_id=current_user.id).one_or_none():
        return redirect(url_for('one_request_mission_confirmation_edit',id=id))
    else :
        return redirect(url_for('new_mission_confirmation_request',id=id))



@app.route('/request/mission-confirmation/requestId=<int:id>', methods=['GET', 'POST'])
@login_required
def one_request_mission_confirmation(id):
    mission_request = Mission_request.query.filter_by(id=id).one_or_none()
    offre = Offre.query.filter_by(id = mission_request.missions_id).one_or_none()
    maitreApprentiStage = Responsable.query.filter_by(id = mission_request.idMaitreApprentiStage).one_or_none()
    professeur = User.query.filter_by(id = mission_request.profReferant_id).one_or_none()
    if not mission_request:
        return redirect(url_for('page_404'))
    if current_user.role_id == 1:
        if mission_request.user_id != current_user.id:
            return redirect(url_for('page_404'))
    mission_request.profReferant = current_user
    db.session.commit()

    return render_template('mission-confirmation-request.html', mission_request=mission_request, offre=offre, professeur=professeur, maitreApprentiStage=maitreApprentiStage)

@app.route('/request/mission-addprod/<int:id>', methods=['POST'])
def add_prof(id):
    mission_request = Mission_request.query.get(id)
    mission_request.profReferant_id = request.form['prof']
    db.session.commit()
    return redirect(url_for('one_request_mission_confirmation', id=id))


@app.route('/my/request/mission-confirmation/requestId=?', methods=['GET', 'POST'])
@login_required
def my_mission_confirmation_request():
    if current_user.role_id != 1:
        return redirect(url_for('page_404'))
    mission_request = Mission_request.query.filter_by(
        user_id=current_user.id).one_or_none()
    if not mission_request or mission_request.user_id != current_user.id:
        return redirect(url_for('page_404'))
    convention = Convention.query.filter_by(user_id=current_user.id).first()
    offre = Offre.query.filter_by(id=mission_request.missions_id).first()
    professeur = User.query.filter_by(id=mission_request.profReferant_id).first()
    maitreApprentiStage = Responsable.query.filter_by(id=mission_request.idMaitreApprentiStage).first()
    return render_template('mission-confirmation-request.html', mission_request=mission_request, convention=convention, offre=offre, professeur=professeur,maitreApprentiStage=maitreApprentiStage)


@app.route('/request/mission-confirmation/requestId=<int:id>/delete', methods=['GET', 'POST'])
@login_required
def one_request_mission_confirmation_delete(id):
    if current_user.role_id != 1:
        return redirect(url_for('page_404'))
    mission_request = Mission_request.query.filter_by(id=id).one_or_none()
    if not mission_request or mission_request.user_id != current_user.id:
        return redirect(url_for('page_404'))
    db.session.delete(mission_request)
    db.session.commit()
    return redirect(url_for('profil'))


@app.route('/request/mission-confirmation/requestId=<int:id>/edit', methods=['GET', 'POST'])
@login_required
def one_request_mission_confirmation_edit(id):
    if current_user.role_id != 1:
        return redirect(url_for('page_404'))
    mission_request = Mission_request.query.filter_by(id=id).one_or_none()
    if not mission_request or mission_request.user_id != current_user.id:
        return redirect(url_for('page_404'))
    if request.method == 'POST':
        contrat_type = request.form.get('offre_type')
        missions = request.form.get('missions')
        duration = request.form.get('duration')
        company_id = request.form.get('company')
        start_at = request.form.get('start-at')
        end_at = request.form.get('end-at')
        salary = request.form.get('salary')
        idMaitreApprentiStage = request.form.get('idMaitreApprentiStage')
        user_id = current_user.id
        mission_request.offre_type_id = contrat_type
        mission_request.missions_id = missions
        mission_request.weeks_number = duration
        mission_request.start_at = datetime.strptime(
            start_at, '%Y-%m-%d')
        mission_request.end_at = datetime.strptime(end_at, '%Y-%m-%d')
        mission_request.salary = salary
        mission_request.idMaitreApprentiStage = idMaitreApprentiStage
        mission_request.user_id = user_id
        mission_request.company_id = company_id
        # mission_request.offre_id = id
        db.session.commit()
        return redirect(url_for('one_request_mission_confirmation', id=mission_request.id))
    offre = Offre.query.filter_by(id=mission_request.missions_id).first()
    return render_template('add_mission-request.html', mission_request=mission_request, offre=offre)


# http://localhost:8000/request/mission-confirmation/requestId=2/validate
@app.route('/request/mission-confirmation/requestId=<int:id>/validate/ask', methods=['GET', 'POST'])
@login_required
def validate_request_mission_ask(id):
    if current_user.role_id == 1:
        return redirect(url_for('page_404'))
    return render_template('accept_mission-request.html', id=id)


@app.route('/request/mission-confirmation/requestId=<int:id>/validate', methods=['GET', 'POST'])
@login_required
def validate_request_mission(id):
    if current_user.role_id == 1:
        return redirect(url_for('page_404'))
    mission_request = Mission_request.query.filter_by(id=id).one_or_none()
    if not mission_request:
        return redirect(url_for('page_404'))
    mission_request.accepted = True
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/requests/mission-confirmation/list', methods=['GET', 'POST'])
@login_required
def list_mission_request():
    if current_user.role_id == 1:
        return redirect(url_for('page_404'))
    mission_requests = Mission_request.query.all()
    for mission_request in mission_requests:
        mission_request.user.promo = Belong.query.filter_by(
            user_id=mission_request.user.id).first().promo
    return render_template('list_mission-request.html', mission_requests=mission_requests)

###################################################
###################################################
###################################################


#                    SOUTENANCE


###################################################
###################################################
###################################################

@app.route("/room/list", methods=['POST', 'GET'])
@login_required
def list_rooms():
    all_rooms = Room.query.order_by(Room.name).all()
    if request.method == 'POST':
        name = request.form.get('name')
        new_room = Room(
            name = name
        )
        db.session.add(new_room)
        db.session.commit()
        return redirect(url_for('list_rooms'))
    
    return render_template("list_rooms.html", rooms = all_rooms)

@app.route("/room/list/<int:id>", methods=['POST'])
@login_required
def delete_room(id):
    if id is not None :
        delete_room_by_id(id)
    return redirect(url_for('list_rooms'))

@app.route('/rooms/update/<int:id>', methods=['POST'])
def update_room(id):
    room = Room.query.get(id)
    if room is None:
        return redirect(url_for('list_rooms'))
    room.name = request.form['name']
    db.session.commit()
    return redirect(url_for('list_rooms'))

@app.route('/soutenance/placing', methods=['GET', 'POST'])
@login_required
def soutenance_placing():
    rooms = Room.query.order_by(Room.name).all();
    #changer les soutenances afin que ça soit uniquement les soutenances non placées
    return render_template("set_soutenance.html", rooms = rooms, soutenances = get_all_soutenance())

@app.route("/soutenance/creer", methods=['GET', 'POST'])
def creer_soutenance():
    if request.method == 'POST':
        candid_teacher_id = request.form.get('profC')
        mission_request_id = request.form.get('Mission_Request')
        soutenanceN = Soutenance(
            date=None,
            duration=None,
            room_id=None,
            candid_teacher_id=candid_teacher_id,
            mission_request_id=mission_request_id
        )
        db.session.add(soutenanceN)
        db.session.commit()
    return render_template("add_soutenance.html",teachers=get_all_teacher(),rooms=get_room(),list_mission_request=get_all_missions_requests())


@app.route("/soutenance/mes", methods=['GET', 'POST'])
def mes_soutenances():
    return render_template("list_soutenances.html",soutenances=get_mes_soutenances(),nbsoutenance=get_nb_soutenance(),current_user = current_user)

@app.route("/soutenance/all", methods=['GET', 'POST'])
def all_soutenance():
    return render_template("list_all-soutenances.html",soutenances=get_all_soutenance(),nbsoutenance=get_nb_soutenance())

@app.route("/soutenance/spc", methods=['GET', 'POST'])
def soutenance_spc():
    print(get_soutenance_spc())
    return render_template("list_spc-soutenances.html",soutenances=get_soutenance_spc(),nbsoutenance=get_nb_soutenance())

@app.route("/soutenance/devenir_candide/<int:id>", methods=['GET'])
@login_required
def devenir_candide(id):
    soutenance = Soutenance.query.filter_by(id = id).first()
    soutenance.candid_teacher = current_user
    db.session.commit()
    return redirect(url_for('mes_soutenances'))


@app.route('/requests/mission-referant/list', methods=['GET', 'POST'])
@login_required
def list_mission_request_referants():
    if current_user.role_id == 1:
        return redirect(url_for('page_404'))
    mission_requests = Mission_request.query.all()
    for mission_request in mission_requests:
        mission_request.user.promo = Belong.query.filter_by(user_id=mission_request.user.id).first().promo
        mission_request.mission = Offre.query.filter_by(id=mission_request.missions_id).first()
    return render_template('list_mission-request-referent.html', mission_requests=mission_requests)


@app.route("/soutenance/all/delete/<int:id>", methods=['GET'])
@login_required
def delete_soutenance(id):
    if id is not None :
        delete_soutenance_by_id(id)
    return redirect(url_for('mes_soutenances'))

@app.route("/soutenance/<int:id>", methods=['GET', 'POST'])
def page_soutenance(id):
    soutenance = Soutenance.query.filter_by(id=id).first()
    return render_template("soutenance.html", soutenance=soutenance)

###################################################
###################################################
###################################################


#                    API


###################################################
###################################################
###################################################


class Company_exists(Resource):
    def get(self, company_serit):
        s = Company.query.filter_by(siret=company_serit).one_or_none()
        if s:
            res = {
                "code": 1,
                "object":  s.serialize()
            }
            return res
        else:
            res = {
                "code": 0,
            }
            return res


class Get_offre_by_ref(Resource):
    def get(self, offre_ref):
        s = Offre.query.filter_by(reference=offre_ref).one_or_none()
        if s:
            res = {
                "code": 1,
                "object":  s.serialize()
            }
            return res
        else:
            res = {
                "code": 0,
            }
            return res


api.add_resource(Company_exists, '/api/company/<int:company_serit>/exists')
api.add_resource(Get_offre_by_ref, '/api/offre/<string:offre_ref>/exists')


###################################################
###################################################
###################################################


#              Helper functions


###################################################
###################################################
###################################################
def delete_soutenance_by_id(id):
    try:
        soutenance = Soutenance.query.filter(Soutenance.id==id).first()
        db.session.delete(soutenance)
        db.session.commit()
    except:
        pass

def delete_room_by_id(id):
    try :
        room = Room.query.filter(Room.id == id).first()
        db.session.delete(room)
        db.session.commit()
    except:
        pass
def get_all_soutenance():
    return Soutenance.query.all()

def get_mes_soutenances():
    soutenance_ref = Soutenance.query.join(Soutenance.mission_request).filter(Mission_request.profReferant_id == current_user.id)
    soutenance_cand = Soutenance.query.filter(Soutenance.candid_teacher == current_user)
    return soutenance_ref.union(soutenance_cand).all()

def get_soutenance_spc():
    soutenance_ref = Soutenance.query.join(Soutenance.mission_request).filter(Mission_request.profReferant_id == current_user.id)
    Soutenance_spc = Soutenance.query.filter_by(candid_teacher_id = "None")
    return Soutenance_spc.except_(soutenance_ref).all()

def get_nb_soutenance():
    return len(Soutenance.query.all())

def get_soutenance_prof(id):
    return Soutenance.query

def get_company_by_id(id):
    return Company.query.filter_by(id=id).first()


def get_user_belongs_to_promo(id):
    return Promo.query.filter_by(id=Belong.query.filter_by(user_id=id).first().promo_id).first()


def is_initiale_by_id(id):
    return Belong.query.filter_by(user_id=id).first().is_initiale


def get_number_offers_from_company(id):
    return len(Offre.query.filter_by(company_id=id).all())


def get_nb_history_by_user_id(id):
    return len(History.query.filter_by(user_id=id).all())


def get_all_activities_dispo():
    return Activity.query.all()


def get_company_list():
    return Company.query.all()


def get_maitreApprentissage_list():
    return Responsable.query.all()


def get_promo_manager_by_id(id):
    return Manage.query.filter_by(promo_id=id).first().user


def get_promo_by_id(id):
    return Manage.query.filter_by(promo_id=id).first()


def get_all_teacher():
    return User.query.filter_by(role_id=2).all()

def get_all_rooms():
    return Room.query.all()

def get_students_by_promo_id(id):
    return set(Belong.query.filter_by(promo_id=id).all())


def user_manage_promo(user_id, promo_id):
    return Manage.query.filter_by(promo_id=promo_id, user_id=user_id).one_or_none()


def get_nb_user_by_promo(promo_id):
    return len(Belong.query.filter_by(promo_id=promo_id).all())


def get_all_promo():
    return Promo.query.all()


def get_status_studenats_by_promo(promo_id):
    belogs = Belong.query.filter_by(promo_id=promo_id).all()
    nb_inisiale = 0
    nb_apprentie = 0
    liste = list()
    liste2 = list()
    nb_stu_nothing = 0
    nb_stu_progress = 0
    nb_stu_accepted = 0

    for belong in belogs:
        if belong.user.status_search == 0:
            nb_stu_nothing += 1
        elif belong.user.status_search == 1:
            nb_stu_progress += 1
        elif belong.user.status_search == 2:
            nb_stu_accepted += 1
        if belong.is_initiale:
            nb_inisiale += 1
        else:
            nb_apprentie += 1
    liste.append(nb_stu_nothing)
    liste.append(nb_stu_progress)
    liste.append(nb_stu_accepted)
    liste2.append(nb_inisiale)
    liste2.append(nb_apprentie)
    final_data = list()
    final_data.append(liste)
    final_data.append(liste2)
    return final_data


def get_anser_by_anser_status(nb):
    if nb == 0:
        return "Pas encore candidaté"
    elif nb == 1:
        return "En cours"
    elif nb == 2:
        return "Accepté(e)"
    else:
        return "Réfusé(e)"


def get_status_company_by_number(nb):
    if nb == 0:
        return "En Attente"
    elif nb == 1:
        return "Acceptée"
    else:
        return "Réfusée"


def is_problem_solved(problem_id):
    return Solved.query.filter_by(problem_id=problem_id).one_or_none() != None


def get_anser_history_by_user_id(id):
    res = 0
    for i in History.query.filter_by(user_id=id).all():
        if i.anser == 1:  # en cour
            res += 1
        elif i.anser == 2:  # accepté
            res += 1
        else:
            # refusé
            res = 0
    return res


def get_promos():
    return Promo.query.all()


def get_nb_validation_by_promo(promo_id):
    manage = Manage.query.filter_by(promo_id=promo_id).all()
    nb = 0
    for m in manage:
        if m.user.status_search == 2:
            nb += 1
    return nb


def get_nb_offre_tobe_validate():
    if current_user.role_id == 1:
        pass
    else:
        return len(Offre.query.filter_by(is_accepted=False).all())
    
def get_nb_offre_sans_profreferant():
    if current_user.role_id == 1:
        pass
    else:
        return len(Mission_request.query.filter_by(profReferant_id=None,accepted=1).all())
    
def getListeApprentiStage(idProf):
    if current_user.role_id != 2:
        pass
    else:
        mission_requests = Mission_request.query.filter_by(profReferant_id=idProf).all()
        for mission_request in mission_requests:
            mission_request.user.promo = Belong.query.filter_by(user_id=mission_request.user.id).first().promo
        return mission_requests

def getPromoParApprentiStage(idProf,typeOffre):
    mission_requests = getListeApprentiStage(idProf)
    dictPromo = {}
    for request in mission_requests:
        if request.offre_type_id == typeOffre:
            dictPromo[request.user.promo.name] = dictPromo.setdefault(request.user.promo.name,0) + 1
    return dictPromo

def getListeSoutenance(idProf):
    if current_user.role_id != 2:
        pass
    else:
        soutenances = Soutenance.query.filter_by(candid_teacher_id=idProf).all()
        for soutenance in soutenances:
            soutenance.mission_request.user.promo = Belong.query.filter_by(user_id=soutenance.mission_request.user.id).first().promo
        return soutenances

def getPromoParSoutenance(idProf):
    soutenances = getListeSoutenance(idProf)
    dictPromo = {}
    for soutenance in soutenances:
        dictPromo[soutenance.mission_request.user.promo.name] = dictPromo.setdefault(soutenance.mission_request.user.promo.name,0) + 1
    return dictPromo

def get_nb_request_tobe_validate():
    if current_user.role_id == 1:
        pass
    else:
        return len(Mission_request.query.filter_by(accepted=False).all())


def get_nb_pb():
    if current_user.role_id == 1:
        pass
    else:
        return len(Problem.query.filter_by(is_solved=False).all())


def get_nb_total_validation():
    return (get_nb_offre_tobe_validate() + get_nb_request_tobe_validate() + get_nb_convention() + get_nb_offre_sans_profreferant())


def get_nb_total_validation_prof():
    return (get_nb_offre_tobe_validate() + get_nb_request_tobe_validate() + get_nb_offre_sans_profreferant())


def get_nb_convention():
    if current_user.role_id == 1:
        pass
    else:
        return len(Convention.query.filter_by(status=0).all())


def user_contrat_etap(user_id):
    pass


def get_promo_name_by_user_id(user_id):
    return Belong.query.filter_by(user_id=user_id).first().promo.name

def get_room():
    return Room.query.all()

def get_all_missions_requests():
    return Mission_request.query.all()

def get_all_activites():
    return Activity.query.all()

def am_i_stagiaire():
    if current_user.role_id != 1:
        return False
    else:
         if Belong.query.filter_by(user_id = current_user.id).first().is_initiale == True : return 1
         else: return 2


app.jinja_env.globals.update(
    is_initiale_by_id=is_initiale_by_id,
    is_problem_solved=is_problem_solved,
    user_manage_promo=user_manage_promo,
    user_contrat_etap=user_contrat_etap,
    am_i_stagiaire = am_i_stagiaire,
    get_company_by_id=get_company_by_id,
    get_company_list=get_company_list,
    get_maitreApprentissage_list=get_maitreApprentissage_list,
    get_user_belongs_to_promo=get_user_belongs_to_promo,
    get_number_offers_from_company=get_number_offers_from_company,
    get_promos=get_promos,
    get_nb_history_by_user_id=get_nb_history_by_user_id,
    get_anser_by_anser_status=get_anser_by_anser_status,
    get_anser_history_by_user_id=get_anser_history_by_user_id,
    get_all_activities_dispo=get_all_activities_dispo,
    get_status_company_by_number=get_status_company_by_number,
    get_promo_manager_by_id=get_promo_manager_by_id,
    get_all_teacher=get_all_teacher,
    get_all_rooms=get_all_rooms,
    get_promo_by_id=get_promo_by_id,
    get_students_by_promo_id=get_students_by_promo_id,
    get_nb_user_by_promo=get_nb_user_by_promo,
    get_status_studenats_by_promo=get_status_studenats_by_promo,
    get_all_promo=get_all_promo,
    get_nb_validation_by_promo=get_nb_validation_by_promo,
    get_nb_offre_tobe_validate=get_nb_offre_tobe_validate,
    get_nb_offre_sans_profreferant=get_nb_offre_sans_profreferant,
    getListeApprentiStage=getListeApprentiStage,
    getPromoParApprentiStage=getPromoParApprentiStage,
    getListeSoutenance=getListeSoutenance,
    getPromoParSoutenance=getPromoParSoutenance,
    get_nb_request_tobe_validate=get_nb_request_tobe_validate,
    get_nb_pb=get_nb_pb,
    get_nb_convention=get_nb_convention,
    get_nb_total_validation=get_nb_total_validation,
    get_promo_name_by_user_id=get_promo_name_by_user_id,
    get_all_activites=get_all_activites,
    get_nb_total_validation_prof=get_nb_total_validation_prof,
)
