from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os.path import join, dirname, realpath
import os
from flask_moment import Moment
from flask_restful import Api


app = Flask(__name__)
app.config.from_object('config')
UPLOAD_FOLDER = join(dirname(realpath(__name__)), "static\images")
ALLOWED_EXTENSIONS = {"png", "jpg", "jpeg"}
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER


db = SQLAlchemy(app)
moment = Moment(app)
api = Api(app)
