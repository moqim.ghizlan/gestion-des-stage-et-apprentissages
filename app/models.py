from sqlalchemy.sql import func
from werkzeug.security import generate_password_hash
from flask_login import UserMixin
from .app import db
import datetime as dt


class User(UserMixin, db.Model):
    __tablename__ = 'User'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(256))
    last_name = db.Column(db.String(256 ))
    email = db.Column(db.String(256), unique=True)
    numero = db.Column(db.String(256), unique=True)
    password = db.Column(db.String(256))
    # 0 == nothing, 1 == in progress, 2 == accepted
    status_search = db.Column(db.Integer, default=0)
    role_id = db.Column(db.Integer)
    role = db.relationship("Role", foreign_keys=[
                           role_id], primaryjoin='Role.id == User.role_id')

    def __repr__(self):
        # return f"User ({self.id}, {self.numero}, {self.first_name} {self.last_name}, {self.email}\n)"
        return f"({self.first_name} {self.last_name})"

    def serialize(self):
        return {
            "id": self.id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "email": self.email,
            "numero": self.numero,
            "status_search": self.status_search,
        }


class Role(db.Model):
    __tablename__ = 'Users_role'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250))

    def __repr__(self):
        return f"Role ({self.id}, {self.name}\n)"

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
        }


class Belong(db.Model):
    __tablename__ = 'Belong'
    id = db.Column(db.Integer, primary_key=True)
    promo_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    is_initiale = db.Column(db.Boolean, default=True)
    promo = db.relationship("Promo", foreign_keys=[promo_id], primaryjoin='Promo.id == Belong.promo_id')
    user = db.relationship("User", foreign_keys=[ user_id], primaryjoin='User.id == Belong.user_id')

    def __repr__(self):
        return f"Belong (User  [id:{self.user_id}] belongs to [Promo id:{self.promo_id}]\n)"

    def serialize(self):
        return {
            "id": self.id,
            "promo": self.promo.serialize(),
            "user": self.user.serialize(),
            "is_initiale": self.is_initiale,
        }


class Manage(db.Model):
    __tablename__ = 'Manage'
    id = db.Column(db.Integer, primary_key=True)
    promo_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    promo = db.relationship("Promo", foreign_keys=[
                            promo_id], primaryjoin='Promo.id == Manage.promo_id')
    user = db.relationship("User", foreign_keys=[
                           user_id], primaryjoin='User.id == Manage.user_id')

    def __repr__(self):
        return f"Manage (User  [id:{self.user_id}] manages the [Promo id:{self.promo_id}]\n)"


class Offre_type(db.Model):
    __tablename__ = 'Offre_type'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250))

    def __repr__(self):
        return f"Role ({self.id}, {self.type}\n)"


class Promo(db.Model):
    __tablename__ = 'Promo'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))

    def __repr__(self):
        return f"Promo ({self.id}, {self.name}\n)"

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
        }


class Company(db.Model):
    __tablename__ = 'Company'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    contact_email = db.Column(db.String(256))
    contact_phone = db.Column(db.String(256))
    ville = db.Column(db.String(256))
    cpostal = db.Column(db.Integer)
    siret = db.Column(db.String(256))
    address = db.Column(db.String(256))
    responsable_id = db.Column(db.Integer)
    responsable = db.relationship("Responsable", foreign_keys=[
                                  responsable_id], primaryjoin='Responsable.id == Company.responsable_id')

    def __repr__(self):
        return f"Company ({self.id}, {self.name}\n)"

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "contact_email": self.contact_email,
            "contact_phone": self.contact_phone,
            "ville": self.ville,
            "cpostal": self.cpostal,
            "siret": self.siret,
            "address": self.address,
            # "is_accepte": self.is_accepte,
            "responsable": self.responsable.serialize(),
        }


class Responsable(db.Model):
    __tablename__ = 'Responsable_company'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(250))
    last_name = db.Column(db.String(250))
    email = db.Column(db.String(250))
    phone_number = db.Column(db.String(250))
    idCompany = db.Column(db.Integer)
    responsable = db.relationship("Company", foreign_keys=[
                                  idCompany], primaryjoin='Company.id == Responsable.idCompany')

    def __repr__(self):
        return f"Responsable ({self.id}, {self.first_name} {self.last_name}, {self.email}\n)"

    def serialize(self):
        return {
            "id": self.id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "email": self.email,
            "phone_number": self.phone_number,
        }


class Activity(db.Model):
    __tablename__ = 'Activity_company'
    id = db.Column(db.Integer, primary_key=True)
    domaine = db.Column(db.String(250))

    def __repr__(self):
        return f"Activity ({self.id}, {self.domaine}\n)"


class Do_activity(db.Model):
    __tablename__ = 'Do_activity'
    id = db.Column(db.Integer, primary_key=True)
    company_id = db.Column(db.Integer)
    activity_id = db.Column(db.Integer)
    company = db.relationship("Company", foreign_keys=[
                              company_id], primaryjoin='Company.id == Do_activity.company_id')
    activity = db.relationship("Activity", foreign_keys=[
                               activity_id], primaryjoin='Activity.id == Do_activity.activity_id')

    def __repr__(self):
        return f"Do_activity (company  [id:{self.company_id}] DO [activity id:{self.activity_id}]\n)"


class Offre(db.Model):
    __tablename__ = 'Offre'
    id = db.Column(db.Integer, primary_key=True)
    titre = db.Column(db.String(256))
    description = db.Column(db.String(256))
    resumerMission = db.Column(db.String(256))
    publication_date = db.Column(db.DateTime(
        timezone=True), server_default=func.now())
    weeks_number = db.Column(db.Integer)
    company_id = db.Column(db.Integer)
    offre_type_id = db.Column(db.Integer)
    reference = db.Column(db.String(256), default=f'@REF-')
    company = db.relationship("Company", foreign_keys=[company_id], primaryjoin='Company.id == Offre.company_id')
    offre_type = db.relationship("Offre_type", foreign_keys=[offre_type_id], primaryjoin='Offre_type.id == Offre.offre_type_id')
    is_token = db.Column(db.Boolean, default=False)
    is_accepted = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f'Offre ({self.id}, {self.titre}, {self.publication_date})'

    def serialize(self):
        return {
            "id": self.id,
            "titre": self.titre,
            "description": self.description,
            "weeks_number": self.weeks_number,
            "company": self.company.serialize(),
            "offre_type_id": self.offre_type_id,
        }


class History(db.Model):
    __tablename__ = 'History'
    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
    send_date = db.Column(db.String(256))
    anser = db.Column(db.Integer)

    offre_id = db.Column(db.Integer)
    company_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)

    company = db.relationship("Company", foreign_keys=[
                              company_id], primaryjoin='Company.id == History.company_id')
    user = db.relationship("User", foreign_keys=[
                           user_id], primaryjoin='User.id == History.user_id')
    offre = db.relationship("Offre", foreign_keys=[
                            offre_id], primaryjoin='Offre.id == History.offre_id')

    def __repr__(self):
        return f'History ({self.id}, [User : {self.user_id}], [Offre : {self.offre_id}], [Company : {self.company_id}], [Send_date : {self.send_date}], [Anser : {self.anser}])'


class Problem(db.Model):
    __tablename__ = "Problem"
    id = db.Column(db.Integer, primary_key=True)
    titre = db.Column(db.String(255))
    description = db.Column(db.String(255))
    link = db.Column(db.String(255))
    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
    reporter_id = db.Column(db.Integer)
    reporter = db.relationship("User", foreign_keys=[
                               reporter_id], primaryjoin='User.id == Problem.reporter_id')
    is_solved = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f'Problem ({self.id}, {self.titre}, {self.description}, {self.link}, {self.created}, {self.reporter_id} , {self.is_solved})'


class Solved(db.Model):
    __tablename__ = "Solved"
    id = db.Column(db.Integer, primary_key=True)
    solved_at = db.Column(db.DateTime(timezone=True),
                          server_default=func.now())
    problem_id = db.Column(db.Integer)
    solver_id = db.Column(db.Integer)
    problem = db.relationship("Problem", foreign_keys=[
                              problem_id], primaryjoin='Problem.id == Solved.problem_id')
    solver = db.relationship("User", foreign_keys=[
                             solver_id], primaryjoin='User.id == Solved.solver_id')

    def __repr__(self):
        return f'Solved ({self.id}, {self.problem_id}, {self.solver_id})'


class Mission_request(db.Model):
    __tablename__ = "Mission_request"
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())
    user_id = db.Column(db.Integer)
    profReferant_id = db.Column(db.Integer)
    company_id = db.Column(db.Integer)
    idMaitreApprentiStage = db.Column(db.Integer)
    accepted = db.Column(db.Boolean, default=False)
    offre_type_id = db.Column(db.String(255))
    weeks_number = db.Column(db.Integer)
    missions_id = db.Column(db.Integer)
    start_at = db.Column(db.Date())
    end_at = db.Column(db.Date())
    salary = db.Column(db.Float)
    offre_type = db.relationship("Offre_type", foreign_keys=[ offre_type_id], primaryjoin='Offre_type.id == Mission_request.offre_type_id')
    user = db.relationship("User", foreign_keys=[ user_id], primaryjoin='User.id == Mission_request.user_id')
    profReferant = db.relationship("User", foreign_keys=[ profReferant_id], primaryjoin='User.id == Mission_request.profReferant_id')
    company = db.relationship("Company", foreign_keys=[ company_id], primaryjoin='Company.id == Mission_request.company_id')
    idResponsable = db.relationship("Responsable", foreign_keys=[idMaitreApprentiStage], primaryjoin='Responsable.id == Mission_request.idMaitreApprentiStage')
    offre = db.relationship("Offre", foreign_keys=[ missions_id], primaryjoin='Offre.id == Mission_request.missions_id')

    def __repr__(self):
        return f'Mission Request ({self.id}, {self.user_id}, {self.company_id})'


class Convention(db.Model):
    __tablename__ = "Convention"
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())
    user_id = db.Column(db.Integer)
    company_id = db.Column(db.Integer)
    mission_request_id = db.Column(db.Integer)
    user = db.relationship("User", foreign_keys=[
                           user_id], primaryjoin='User.id == Convention.user_id')
    company = db.relationship("Company", foreign_keys=[
                              company_id], primaryjoin='Company.id == Convention.company_id')
    mission_request = db.relationship("Mission_request", foreign_keys=[
                                      mission_request_id], primaryjoin='Mission_request.id == Convention.mission_request_id')
    status = db.Column(db.Integer)  # 0 == encoure, 1==acceptee, 2==refusee

    def __repr__(self):
        return f'Mission Request ({self.id}, {self.user_id}, {self.company_id})'

class Room(db.Model):
    __tablename__ = "Room"
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(255))

    def __repr__ (self):
        return f'Room ({self.id}, {self.name})'

class Soutenance(db.Model):
    __tablename__ = "Soutenance"
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime(timezone=True))
    duration = db.Column(db.Integer)    
    room_id = db.Column(db.Integer)
    mission_request_id = db.Column(db.Integer)
    candid_teacher_id = db.Column(db.Integer)

    mission_request = db.relationship("Mission_request", foreign_keys=[
                                        mission_request_id], primaryjoin='Mission_request.id == Soutenance.mission_request_id')
    candid_teacher = db.relationship("User", foreign_keys=[
                                        candid_teacher_id], primaryjoin='User.id == Soutenance.candid_teacher_id')
    room = db.relationship("Room", foreign_keys=[
                                        room_id], primaryjoin='Room.id == Soutenance.room_id')

    def __repr__ (self):
        return f'Soutenance ({self.id}, {self.date}, {self.mission_request_id}, {self.candid_teacher_id}, {self.room_id})'


def __init_app__():
    __init_db__()
    __init_date__()


def __init_db__():
    print('\n'*5)
    print('STARTED')
    db.drop_all()
    print('\n'*5)
    print('TABLES DROPPET')
    db.create_all()
    print('\n'*5)
    print('TABLES CREATED')
    print('\n'*5)


def __init_date__():
    role1 = Role(name='user')
    role2 = Role(name='teacher')
    role3 = Role(name='admin')
    db.session.add(role1)
    db.session.add(role2)
    db.session.add(role3)
    db.session.commit()

    offre_type_stage = Offre_type(name='Stage')
    offre_type_appr = Offre_type(name='Apprentissage')
    db.session.add(offre_type_stage)
    db.session.add(offre_type_appr)
    db.session.commit()


    activity1 = Activity(domaine="Web",)
    activity2 = Activity(domaine="Mobile",)
    activity3 = Activity(domaine="Sécurité",)
    db.session.add(activity1)
    db.session.add(activity2)
    db.session.add(activity3)
    db.session.commit()


    new_admin(first_name="admin", last_name="admin", email="admin", numero="admin",
              password=generate_password_hash('theadmin', method='sha256'), role_id=3)



    print('\n'*5)
    print('DATA INSERTED')


def new_user(first_name, last_name, email, numero, password, status_search, promo_id, role_id=1, is_initiale=True):
    u = User(
        first_name=first_name,
        last_name=last_name,
        email=email,
        numero=numero,
        password=password,
        role_id=role_id,
        status_search=status_search,
    )
    db.session.add(u)
    db.session.commit()
    b = Belong(user_id=u.id, promo_id=promo_id, is_initiale=is_initiale)
    db.session.add(b)
    db.session.commit()


def new_teacher(first_name, last_name, email, numero, password, promo_id, role_id=2):
    u = User(
        first_name=first_name,
        last_name=last_name,
        email=email,
        numero=numero,
        password=password,
        role_id=role_id
    )
    db.session.add(u)
    db.session.commit()
    b = Manage(user_id=u.id, promo_id=promo_id)
    db.session.add(b)
    db.session.commit()


def new_admin(first_name, last_name, email, numero, password, role_id=3):
    u = User(
        first_name=first_name,
        last_name=last_name,
        email=email,
        numero=numero,
        password=password,
        role_id=role_id
    )
    db.session.add(u)
    db.session.commit()


def new_problem(titre, description, link, reporter_id):
    problem = Problem(titre=titre, description=description,
                      link=link, reporter_id=reporter_id)
    db.session.add(problem)
    db.session.commit()


def new_solve_problem(problem_id, solver_id):
    problem = Problem.query.filter_by(id=problem_id).first()
    problem.is_solved = True
    db.session.commit()
    solve = Solved(problem_id=problem.id, solver_id=solver_id)
    db.session.add(solve)
    db.session.commit()
